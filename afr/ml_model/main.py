# data ingestion
import pandas as pd
import numpy as np

# skkkkkkkklearn
from sklearn import base
from sklearn.metrics.pairwise import pairwise_distances
from sklearn.compose import ColumnTransformer

from sklearn.preprocessing import Imputer, Normalizer
from sklearn.pipeline import Pipeline, FeatureUnion

# ruining cucumbers
from pathlib import Path
import pickle


#pipeline
from afr.ml_model.transformer import ColumnSelectTransformer, NormalizeColumnTransformer 

# db access
from afr.utils.db_access import get_db_connection
from afr.utils.helper_functions import lookup_artwork_url, lookup_artist_name

'''
Created 8/1/19 by Nanette.


    > INFO:

    > STRUCTURE:
        1. get_matches
        2. main
'''

QUERY_GET_INPUT_DATA = '''
    SELECT tempo, valence, danceability, energy
    FROM cm_track_audio_feature
    WHERE isrc = %s
'''

QUERY_GET_ISRC = '''
    SELECT isrc
    FROM spotify
    WHERE lower(track_name)=%s
    ORDER BY popularity_score DESC NULLS LAST
    LIMIT 1;
'''

QUERY_GET_NAME = '''
    SELECT track_name, spotify_artist_id
    FROM spotify
    WHERE isrc=%s
    ORDER BY popularity_score DESC NULLS LAST
    LIMIT 1;
'''

GENRES = ['pop', 'classical', 'country', 'jazz', 'rnb']
BASE_PATH = Path(__file__).resolve().parent
DECIMAL_PLACES = 4


'''
Expected return: array of dicts
	dict = {
		isrc: string
		track_name: string
		track_genre: string
		spotify_track_id: string
		duration_ms: int
		popularity: int
		tempo: float
		valence: float
		energy: float
		danceability: float
		loudness: float
		similarity: float (which will give distance)
	}
'''

def get_matches(input_isrc, output_genre, limit=10, random=True):
	#print("Target Genre: %s | ISRC: %s" % (output_genre, input_isrc) )

	# 0. METADATA
	data = _get_data_given_isrc(input_isrc)
	name = data[0].lower() if data else ''
	#artist_id = data[1] if data else ''
	# if data:
	# 	print("Track Name: %s | Spotify Artist ID: %s" % (data[0], artist_id))
	#print()

	# 0.5 PREP THE PICKLES
	pipe_pickle_path = BASE_PATH / 'pickles/pipe.pickle' # joins path
	with open(pipe_pickle_path, "rb") as pickle_pipe:
		shortest_distance_pipe = pickle.load(pickle_pipe)

	# 1. PREPROCESSING
	input_vector = _vectorize_input_song(shortest_distance_pipe, input_isrc)
	output_genre_df = _unpickle_one_dataframe(output_genre)
	output_genre_df = output_genre_df[output_genre_df.isrc != input_isrc]
	output_genre_df = output_genre_df[output_genre_df.track_name.str.lower() != name]
	candidates = shortest_distance_pipe.transform(output_genre_df)

	# 2. SHORTEST DISTANCE ALGO
	distance_results = pairwise_distances(input_vector, candidates, metric='euclidean')
	distance_results_transformed = list(distance_results)[0]   #Reform matches to go into a df (format nicer)
	num_candidates = len(distance_results_transformed)
	#print("> Possible number of candidates:", num_candidates)

	# 3. SELECT MATCHES
	similarity_df = pd.DataFrame({ 'similarity': pd.Series(distance_results_transformed) ,'l_index': range(0,num_candidates)})
	sorted_similarity_df = similarity_df.sort_values('similarity', ascending=True)

	if random: # take random subset of matches
	    #print("> # of semi-randomized matches:", limit)

	    NUM_TOP_MATCHES = limit // 2
	    NUM_BOTTOM_MATCHES = limit - NUM_TOP_MATCHES


	    top_matches = sorted_similarity_df[:NUM_TOP_MATCHES]
	    bottom_matches = sorted_similarity_df[NUM_TOP_MATCHES:limit*2].sample(NUM_BOTTOM_MATCHES)
	    similarity_matches = pd.concat([top_matches, bottom_matches])

	else: # take top entries
	    #print("> # of ordered matches:", limit)
	    similarity_matches = sorted_similarity_df[:limit]

	# 4. PRINT RESULTS
	# print("\n> Matches:")
	min_similarity = similarity_matches['similarity'].min()
	max_similarity = similarity_matches['similarity'].max()

	log_min = 10**min_similarity
	log_max = 10**max_similarity
	min_max_diff = log_max - log_min

	matches = []
	for index, row in similarity_matches.iterrows():
		match_row = output_genre_df.iloc[index].drop(labels=['index'])
		match_dict = match_row.to_dict()
		match_artwork = lookup_artwork_url(match_dict.get('isrc'))
		artist_name = lookup_artist_name(match_dict.get('isrc'))

		popularity = match_dict['popularity'] if match_dict['popularity'] else 0
		similarity = float(row.similarity)
		log_similarity = 10**similarity

		# https://math.stackexchange.com/questions/377169/calculating-a-value-inside-one-range-to-a-value-of-another-range
		node_size = 0.3 * popularity + 20
		edge_length = 60 + ((log_similarity - log_min) / min_max_diff) * 60

		matches += [{**match_dict, 'artist_name': artist_name, 'artwork_url': match_artwork,'similarity': similarity, 'node_size': node_size, 'edge_length': edge_length}]		
		# print(match_row)
		# print(" ")

	#print(matches)
	return matches

'''
HELPER FUNCTIONS
'''

def _unpickle_one_dataframe(genre):
	genre_pickle = f"pickles/{genre}_df.pickle"
	with open(BASE_PATH / genre_pickle,"rb") as df:
		return pickle.load(df)

def _unpickle_dataframes():
	genre_dfs = {}
	for genre in GENRES:
		genre_pickle = f"pickles/{genre}_df.pickle"
		genre_pickle_path = BASE_PATH / genre_pickle
		with open(genre_pickle_path,"rb") as df:
			genre_dfs[genre] = pickle.load(df)

	return genre_dfs

def _get_data_for_input_isrc(input_isrc):
    with get_db_connection() as con:
        cur = con.cursor()
        cur.execute(QUERY_GET_INPUT_DATA, (input_isrc,))
        response = cur.fetchone()
        converted_response = tuple(map(float, response))
        return pd.DataFrame(list([converted_response]), columns=['tempo', 'valence', 'danceability', 'energy'])

def _vectorize_input_song(shortest_distance_pipe, input_isrc):
    input_isrc_df = _get_data_for_input_isrc(input_isrc)
    input_song_vector = shortest_distance_pipe.transform(input_isrc_df)

    #print("> Input Song Vector:", input_song_vector)
    return input_song_vector

def _find_isrc_given_name(name):
    with get_db_connection() as con:
        cur = con.cursor()
        cur.execute(QUERY_GET_ISRC, (name,))
        isrc = cur.fetchone()
        return isrc

def _get_data_given_isrc(isrc):
    with get_db_connection() as con:
        cur = con.cursor()
        cur.execute(QUERY_GET_NAME, (isrc,))
        data = cur.fetchone()
        return data


if __name__ == "__main__":
    input_isrc = 'USUM71514637'
    output_genre = 'jazz'
    limit=10
    random = True

    get_matches(input_isrc, output_genre, limit, random)
