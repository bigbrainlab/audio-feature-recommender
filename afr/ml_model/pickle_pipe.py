'''code to be copied nto main.py and executed at root level that trains a pipe in order to pickle
   in the correct environment'''

used_cols = ['tempo', 'valence', 'danceability', 'energy']

pipe_tempo_normalizer = NormalizeColumnTransformer('tempo')
shortest_distance_pipe = Pipeline([
                        ('column_filter', ColumnSelectTransformer(used_cols)),
                        ('normalize columns', ColumnTransformer([
                           ("tempo normalizer", pipe_tempo_normalizer, [0]), 
                           ("passthrough transformer", 'passthrough', [1, 2, 3])
                       ]))
                    ])


with open(BASE_PATH / "pickles/pop_df.pickle","rb") as df:
	pop_df = pickle.load(df)
with open(BASE_PATH / "pickles/classical_df.pickle","rb") as df:
	classical_df = pickle.load(df)
with open(BASE_PATH / "pickles/jazz_df.pickle","rb") as df:
	jazz_df = pickle.load(df)
with open(BASE_PATH / "pickles/country_df.pickle","rb") as df:
	country_df = pickle.load(df)
with open(BASE_PATH / "pickles/rnb_df.pickle","rb") as df:
	rnb_df = pickle.load(df)

mega_df =  pd.concat([pop_df, classical_df, jazz_df, country_df, rnb_df])
shortest_distance_pipe.fit_transform(mega_df)

pickle_out = open("afr/ml_model/pickles/pipe.pickle","wb")
pickle.dump(shortest_distance_pipe, pickle_out)
pickle_out.close()