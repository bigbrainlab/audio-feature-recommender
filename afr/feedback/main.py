
from afr.utils.db_access import get_db_connection

QUERY_INSERT_STAT = """
	INSERT INTO afr_feedback
	(ip_address, genre, thumbs_up)
	VALUES 
	(%s, %s, %s);
"""

def add_feedback_to_db(ip_address, genre, thumbs_up):
	with get_db_connection() as con:
		cur = con.cursor()
		cur.execute(QUERY_INSERT_STAT, (ip_address, genre, thumbs_up,))
		con.commit()

		print(f"Feedback inserted for {ip_address} | genre: {genre}, thumbs up: {thumbs_up}")

