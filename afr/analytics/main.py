from afr.utils.helper_functions import lookup_cm_track_id, lookup_top_playlist_containing_isrc, lookup_spotify_reach, lookup_num_spotify_playlists, lookup_popularity
from afr.services.spotify import get_thirty_sec_preview_by_isrc

from quantiphy import Quantity
'''
Created on 8/8/19 by Nanette.

structure
* public methods
	get_analytics
* private methods
'''

SPOTIFY_PLAYLIST_BASE_URL = 'https://open.spotify.com/playlist/{playlist_id}'
CHARTMETRIC_BASE_URL = 'https://app.chartmetric.com/track?id={track_id}'
TEST_ISRC = 'USUM71911283' # senorita

def get_analytics(isrc):
	Quantity.set_prefs(prec=1)

	# 1. top spotify playlist isrc is in
	top_spotify_playlist_id = lookup_top_playlist_containing_isrc(isrc)
	top_spotify_playlist_url = SPOTIFY_PLAYLIST_BASE_URL.format(playlist_id=top_spotify_playlist_id) if top_spotify_playlist_id else None

	# 2. chartmetric track page
	cm_track_id = lookup_cm_track_id(isrc)
	cm_track_page_url = CHARTMETRIC_BASE_URL.format(track_id=cm_track_id) if cm_track_id else None

	# 3. spotify data: spotify, apple, amazon
	spotify_reach = lookup_spotify_reach(isrc)
	num_spotify_playlist = lookup_num_spotify_playlists(isrc)
	spotify_popularity = lookup_popularity(isrc)

	# 4. song preview
	thirty_sec_preview = get_thirty_sec_preview_by_isrc(isrc)

	analytics = {
		'spotify_reach': str(Quantity(spotify_reach)) if spotify_reach else None,
		'num_spotify_playlists': str(Quantity(num_spotify_playlist)) if num_spotify_playlist else None,
		'spotify_popularity': spotify_popularity,
		'cm_track_page': cm_track_page_url,
		'top_spotify_playlist_url':top_spotify_playlist_url,
		'thirty_sec_preview': thirty_sec_preview
	}

	return analytics

