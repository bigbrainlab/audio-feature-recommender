from flask import Flask, jsonify, request
from flask_cors import CORS
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address

import nujson

from afr.analytics.main import get_analytics
from afr.feedback.main import add_feedback_to_db
from afr.ml_model.main import get_matches
from afr.services.spotify import get_song_data_by_name, get_song_data_by_spotify_id
from afr.utils.helper_functions import lookup_artwork_url, lookup_track_name

app = Flask(__name__)
CORS(app)
limiter = Limiter(app, key_func=get_remote_address)

DEFAULT_LIMIT = 5
DEFAULT_RANDOM = True

@app.route('/', methods = ['GET'])
def test():
	if request.method == 'GET':
		return "eThAn iS tHe SiLliEsT GoOsE (˶‾᷄ ⁻̫ ‾᷅˵)"

@app.route('/recommend', methods = ['GET'])
#@limiter.limit("100 per hour")
def recommend_songs():
	if request.method == 'GET':
		"""find matches for input song (isrc) in desired genre"""
		isrc = request.args.get('isrc')
		genre = request.args.get('genre')
		if not isrc or not genre:
			return jsonify({'error': 'Missing ISRC and/or Genre'}), 422 # unprocessable entity error

		limit = request.args.get('limit') if request.args.get('limit') else DEFAULT_LIMIT
		random = request.args.get('random') if request.args.get('random') else DEFAULT_RANDOM

		source_data = {'isrc':isrc, 'track_name':lookup_track_name(isrc), 'artwork_url':lookup_artwork_url(isrc)}
		matches = get_matches(isrc, genre, limit, random)

		return nujson.dumps({'source':source_data, 'matches':matches})

# @app.route('/sample/<string:name>/', methods = ['GET'])
# #@limiter.limit("100 per minute")
# def sample_song_by_name(name):
# 	if request.method == 'GET':
# 		data = get_song_data_by_name(name)
# 		return jsonify(data), 

@app.route('/sample', methods = ['GET'])
#@limiter.limit("100 per minute")
def sample_song():
	if request.method == 'GET':
		name = request.args.get('name')
		spotify_track_id = request.args.get('spotify_id')

		if name:
			data = get_song_data_by_name(name)
			if not data:
				return jsonify({'message': f'could not find isrc/embed url for {name}'}), 204
			return jsonify(data), 200

		elif spotify_track_id:
			data = get_song_data_by_spotify_id(spotify_track_id)
			if not data:
				return jsonify({'message': f'could not find isrc/embed url for {spotify_track_id}'}), 204
			return jsonify(data), 200

		return jsonify({'error': 'silly goose, i need input data'}), 400

@app.route('/analytics/<string:isrc>', methods = ['GET'])
#@limiter.limit("100 per minute")
def analyze_song(isrc):
	if request.method == 'GET':
		analytics_dict = get_analytics(isrc)
		return jsonify(analytics_dict)

@app.route('/feedback/add', methods = ['POST'])
#@limiter.limit("100 per hour")
def add_feedback():
	if request.method == 'POST':
		source_data = request.get_json(force=True)

		ip_address = source_data.get('ip_address')
		genre = source_data.get('genre')
		thumbs_up = source_data.get('thumbs_up')

		if genre and isinstance(thumbs_up, bool):
			add_feedback_to_db(ip_address, genre, thumbs_up)
			return jsonify({'success': True})

		return jsonify({'error': 'silly goose, need valid input (ip_address (optional), genre, thumbs_up)'}), 400







