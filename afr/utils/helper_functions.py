from afr.utils.db_access import get_db_connection

QUERY_FIND_NAME_BY_ISRC = '''
	SELECT track_name
	FROM spotify
	WHERE isrc=%s
	ORDER BY popularity_score DESC NULLS LAST 
	LIMIT 1;
'''

BAD_QUERY_HECK = '''
	SELECT isrc
	FROM spotify
	WHERE LOWER_UNACCENT(TRIM(REGEXP_REPLACE(track_name, '\(.*\)', ''))) LIKE %s
	ORDER BY popularity_score DESC NULLS LAST 
	LIMIT 1;
'''

QUERY_FIND_ISRC_BY_NAME = '''
	WITH tracks AS (
		SELECT id, isrc
		FROM cm_track
		WHERE LOWER_UNACCENT(name) LIKE LOWER_UNACCENT(%s::text || '%%')
		LIMIT 1000
	), values AS (
		SELECT t.isrc, (
			SELECT MAX(popularity_score)
			FROM spotify
			WHERE isrc = t.isrc
		) AS score
		FROM tracks t
		ORDER BY score DESC NULLS LAST
		LIMIT 1
	)
	SELECT isrc from values;
'''

QUERY_FIND_ISRC_BY_SPOTIFY_ID = '''
	SELECT isrc
	FROM spotify
	WHERE spotify_track_id=%s
	ORDER BY popularity_score DESC NULLS LAST 
	LIMIT 1;
'''

QUERY_GET_SPOTIFY_ID = '''
	SELECT spotify_track_id
	FROM spotify
	WHERE isrc=%s
	ORDER BY popularity_score DESC NULLS LAST
	LIMIT 1;
'''

QUERY_GET_ARTWORK_URL = '''
	SELECT artwork_url
	FROM spotify
	WHERE isrc=%s
	ORDER BY popularity_score DESC NULLS LAST
	LIMIT 1;
'''

QUERY_GET_ARTIST_NAME = '''
	SELECT artist_name
	FROM spotify
	WHERE isrc=%s; 
'''

QUERY_GET_TOP_PLAYLIST = '''
	WITH playlists AS (
		SELECT lsp.spotify_playlist
		FROM spotify
		INNER JOIN l_spotify_playlist lsp ON spotify.id = lsp.spotify
		WHERE isrc = %s
	), top_playlist AS (
		SELECT p.spotify_playlist
		FROM playlists p
		INNER JOIN spotify_playlist_stat_cache spsc ON spsc.id = p.spotify_playlist
		ORDER BY spsc.followers DESC NULLS LAST
		LIMIT 1
	) SELECT sp.playlist_id
	  FROM spotify_playlist sp
	  WHERE sp.id = (
	  	SELECT spotify_playlist
	  	FROM top_playlist
	 )
'''

QUERY_GET_CM_TRACK_ID = '''
	SELECT id
	FROM cm_track
	WHERE isrc=%s
'''

QUERY_GET_REACH = '''
	WITH followers AS (
	    SELECT sp.followers_latest
	    FROM spotify s
	    INNER JOIN l_spotify_playlist lsp ON lsp.spotify = s.id
	    INNER JOIN spotify_playlist sp ON lsp.spotify_playlist = sp.id
	    WHERE s.isrc = %s
	    GROUP BY sp.id
	)
	SELECT SUM(followers_latest) as sum
	FROM followers;
'''

QUERY_GET_NUM_SPOTIFY_PLAYLISTS = '''
	SELECT COUNT(DISTINCT spotify_playlist)
	FROM l_spotify_playlist lsp
	INNER JOIN spotify ON spotify.id = lsp.spotify
	WHERE isrc = %s;
'''

QUERY_GET_POPULARITY_SCORE = '''
	SELECT popularity_score
	FROM spotify
	WHERE isrc = %s 
	ORDER BY popularity_score
	LIMIT 1;
'''

def lookup_isrc(name):
	with get_db_connection() as con:
		cur = con.cursor()
		cur.execute(QUERY_FIND_ISRC_BY_NAME, (name,))
		isrc_tuple = cur.fetchone()
		return isrc_tuple[0] if isrc_tuple else None

def lookup_isrc_by_spotify_id(spotify_id):
	with get_db_connection() as con:
		cur = con.cursor()
		cur.execute(QUERY_FIND_ISRC_BY_SPOTIFY_ID, (spotify_id,))
		isrc_tuple = cur.fetchone()
		return isrc_tuple[0] if isrc_tuple else None

def lookup_track_name(isrc):
	with get_db_connection() as con:
		cur = con.cursor()
		cur.execute(QUERY_FIND_NAME_BY_ISRC, (isrc,))
		name_tuple = cur.fetchone()
		return name_tuple[0] if name_tuple else None

def lookup_spotify_track_id(isrc):
	with get_db_connection() as con:
		cur = con.cursor()
		cur.execute(QUERY_GET_SPOTIFY_ID, (isrc,))
		track_id_tuple = cur.fetchone()
		return track_id_tuple[0] if track_id_tuple else None

def lookup_artwork_url(isrc):
	with get_db_connection() as con:
		cur = con.cursor()
		cur.execute(QUERY_GET_ARTWORK_URL, (isrc,))
		artwork_tuple = cur.fetchone()
		return artwork_tuple[0] if artwork_tuple else None

def lookup_artist_name(isrc):
	with get_db_connection() as con:
		cur = con.cursor()
		cur.execute(QUERY_GET_ARTIST_NAME, (isrc,))
		artist_tuple = cur.fetchone()
		if artist_tuple:
			artist = artist_tuple[0]
			cleaned_artist = artist.replace('Anonymous,', '').replace(',Anonymous', '').replace(',', ', ').strip() if artist else None
			return cleaned_artist
		return None

'''ANALYTICS'''
def lookup_top_playlist_containing_isrc(isrc):
	with get_db_connection() as con:
		cur = con.cursor()
		cur.execute(QUERY_GET_TOP_PLAYLIST, (isrc,))
		playlist_id_tuple = cur.fetchone()
		return playlist_id_tuple[0] if playlist_id_tuple else None

def lookup_cm_track_id(isrc):
	with get_db_connection() as con:
		cur = con.cursor()
		cur.execute(QUERY_GET_CM_TRACK_ID, (isrc,))
		cm_track_tuple = cur.fetchone()
		return cm_track_tuple[0] if cm_track_tuple else None

def lookup_spotify_reach(isrc):
	with get_db_connection() as con:
		cur = con.cursor()
		cur.execute(QUERY_GET_REACH, (isrc,))
		reach_tuple = cur.fetchone()
		return reach_tuple[0] if reach_tuple else None

def lookup_num_spotify_playlists(isrc):
	with get_db_connection() as con:
		cur = con.cursor()
		cur.execute(QUERY_GET_NUM_SPOTIFY_PLAYLISTS, (isrc,))
		playlists_tuple = cur.fetchone()
		return playlists_tuple[0] if playlists_tuple else None

def lookup_popularity(isrc):
	with get_db_connection() as con:
		cur = con.cursor()
		cur.execute(QUERY_GET_POPULARITY_SCORE, (isrc,))
		popularity_tuple = cur.fetchone()
		return popularity_tuple[0] if popularity_tuple else None

# lookup_isrc('ethan is a silly goose what in the world this is the longest query in the world!!!9582308502398509285')
