from sklearn import base

class ColumnSelectTransformer(base.BaseEstimator, base.TransformerMixin):
    """
    A transformer to pull columns out of a dataframe.
    Input: list of column names.
    Output: X filtered to have just those columns.
    """

    def __init__(self, col_names):
        self.col_names = col_names  # We will need these in transform()

    def fit(self, X, y=None):
        # This transformer doesn't need to learn anything about the data,
        # so it can just return self without any further processing
        return self

    def transform(self, X):
        # Return an array with the same number of rows as X and one
        # column for each in self.col_names
        return X[self.col_names].copy()
        
class NormalizeColumnTransformer(base.BaseEstimator, base.TransformerMixin):
    """
    A transformer to pull columns out of a dataframe.
    Input: list of column names.
    Output: X filtered to have just those columns.
    """

    def __init__(self, target_col):
        self.target_col = target_col
        self.min_ = None
        self.max_ = None

    def fit(self, X, y=None):
        x_min = X[self.target_col].min()
        x_max = X[self.target_col].max()
        
        self.min_ = x_min if not self.min_ or x_min < self.min_ else self.min_
        self.max_ = x_max if not self.max_ or x_max > self.max_ else self.max_        
        return self
        
    def transform(self, X):
        # Return an array with the same number of rows as X and one
        # column for each in self.col_names
        
        column_vals = X[self.target_col]
        diff_cols_min = (column_vals - self.min_)
        diff_max_min = (self.max_ - self.min_) 
        
        X_new = X.copy()
        X_new[self.target_col] = diff_cols_min / diff_max_min
        
        return X_new.to_numpy()