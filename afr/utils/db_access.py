from contextlib import contextmanager
import psycopg2
import psycopg2.extras
from psycopg2.pool import ThreadedConnectionPool
import os

psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)

MAIN_DB_CONFIGS = {
    "database": os.environ.get("DB_NAME"),
    "user": os.environ.get("DB_USR"),
    "password": os.environ.get("DB_PSWD"),
    "host": os.environ.get("DB_HOST"),
    "port": int(os.environ.get("DB_PORT")),
}
SLAVE_DB_CONFIGS = {
    "database": os.environ.get("DB_NAME"),
    "user": os.environ.get("DB_USR"),
    "password": os.environ.get("DB_PSWD"),
    "host": os.environ.get("DB_HOST"),
    "port": int(os.environ.get("DB_PORT")),
}
MAX_CLIENTS = 50

poolMaster = ThreadedConnectionPool(
    1,
    MAX_CLIENTS,
    database=MAIN_DB_CONFIGS["database"],
    user=MAIN_DB_CONFIGS["user"],
    password=MAIN_DB_CONFIGS["password"],
    host=MAIN_DB_CONFIGS["host"],
    port=MAIN_DB_CONFIGS["port"],
)
poolSlave = ThreadedConnectionPool(
    1,
    MAX_CLIENTS,
    database=SLAVE_DB_CONFIGS["database"],
    user=SLAVE_DB_CONFIGS["user"],
    password=SLAVE_DB_CONFIGS["password"],
    host=SLAVE_DB_CONFIGS["host"],
    port=SLAVE_DB_CONFIGS["port"],
)


@contextmanager
def get_db_connection(master=True):
    try:
        connection = poolMaster.getconn() if master else poolSlave.getconn()
        yield connection
    finally:
        if master:
            poolMaster.putconn(connection)
        else:
            poolSlave.putconn(connection)


@contextmanager
def get_db_cursor(commit=False):
    with get_db_connection() as connection:
        cursor = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        try:
            yield cursor
            if commit:
                connection.commit()
        finally:
            cursor.close()


def db_connect():
    return psycopg2.connect(
        database=MAIN_DB_CONFIGS["database"],
        user=MAIN_DB_CONFIGS["user"],
        password=MAIN_DB_CONFIGS["password"],
        host=MAIN_DB_CONFIGS["host"],
        port=MAIN_DB_CONFIGS["port"],
    )


def db_disconnect(con):
    if con:
        con.close()


if __name__ == "__main__":

    """
        Getting cursor object from pool
    """
    with get_db_cursor() as cursor:
        cursor.execute("select 1")
        data = cursor.fetchall()
        print(data)
    """
        Getting connection object from pool
    """
    with get_db_connection(master=True) as con:
        cur = con.cursor()
        cur.execute(
            """
            SELECT * FROM pg_stat_activity
        """
        )
        data = cur.fetchall()
        print(data)
