import requests
import json

from afr.utils.helper_functions import lookup_isrc, lookup_spotify_track_id, lookup_isrc_by_spotify_id

TRACK_URL = 'https://open.spotify.com/embed/track/{track_id}'
TEST_SONG_NAME = 'Cake By The Ocean'

AUTH_URL = 'https://open.spotify.com/browse/featured'
API_URL = 'https://api.spotify.com/v1/tracks/{track_id}'

BROWSER_HEADER = {
    'Referer': 'https://open.spotify.com/browse',
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36'
}


'''
Created on 8/8/19 by Nanette.

structure
* public methods
    get_song_data
    get_song_data_by_isrc
* private methods
    _get_api_response
    _get_auth
    _get_cookies
    _parse_to_json
'''

def get_song_data_by_spotify_id(spotify_track_id):
    isrc = lookup_isrc_by_spotify_id(spotify_track_id)
    if not isrc:
        return None

    url = TRACK_URL.format(track_id=spotify_track_id) 
    return {'isrc': isrc, 'embed_url': url}

def get_song_data_by_name(name):
    # 1. find isrc; if cannot find, return None
    isrc = lookup_isrc(name)
    if not isrc:
        return None

    # 2. generate url 
    spotify_track_id = lookup_spotify_track_id(isrc)
    if not spotify_track_id:
        return {'isrc':isrc}

    url = TRACK_URL.format(track_id=spotify_track_id) 
    return {'isrc': isrc, 'embed_url': url}

def get_song_data_by_isrc(isrc):
    spotify_track_id = lookup_spotify_track_id(isrc)
    url = TRACK_URL.format(track_id=spotify_track_id) if spotify_track_id else None
    
    return {'embed_url': url}

def get_thirty_sec_preview_by_isrc(isrc):
    # 1. generate url to make spotify API call
    spotify_track_id = lookup_spotify_track_id(isrc)
    url = API_URL.format(track_id=spotify_track_id)
    
    # 2. make api call; if invalid, return None
    response = _get_api_response(url)
    if not response:
        return

    # 3. parse url -> 30 sec song snippet
    preview_url = response.get('preview_url')

    return preview_url


'''HELPER FUNCTIONS'''
def _get_api_response(url):
    data = requests.get(url, headers={'Authorization': _get_auth()})
    return _parse_to_json(data.text)

def _get_auth():
    cookies = _get_cookies()
    if cookies and 'wp_access_token' in cookies:
        token = cookies['wp_access_token']
        return "Bearer " + token
    else:
        print("FAILED. Could not get token.")
        return None

def _get_cookies():
    with requests.Session() as session:
        session.get(AUTH_URL, headers=BROWSER_HEADER)
        return session.cookies.get_dict()

def _parse_to_json(response):
    if not response:
        return None

    try:
        return json.loads(response)
    except:
        print("Unable to parse response to json.")
        return None

if __name__ == "__main__":
    print(get_song_data(TEST_SONG_NAME))

