FROM python:3.7

MAINTAINER Chartmetric "hi@chartmetric.io"

RUN mkdir ./app
COPY . ./app
WORKDIR ./app

RUN pip install -r requirements.txt

CMD gunicorn -b :$PORT --workers 1 --threads 8 wsgi:app
