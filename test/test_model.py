from afr.ml_model.main import get_matches
import pytest

TEST_ISRC_PIECE_OF_YOUR_HEART = 'DEUM71807719'
TEST_ISRC_TRUTH_HURTS = 'USAT21703896'

TEST_GENRE_POP = 'pop'
TEST_GENRE_JAZZ = 'jazz'

TEST_LIMIT = 5

TEST_RANDOM_FALSE = False

'''
Created on 8/23/19 by Nanette.
'''

def test_matches_piece_of_your_heart():
	matches = get_matches(TEST_ISRC_PIECE_OF_YOUR_HEART, TEST_GENRE_POP)
	assert matches
	assert len(matches) > 0

def test_matches_truth_hurts():
	matches = get_matches(TEST_ISRC_TRUTH_HURTS, TEST_GENRE_JAZZ, TEST_LIMIT, TEST_RANDOM_FALSE)
	assert matches
	assert len(matches) > 0