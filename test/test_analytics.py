from afr.analytics.main import get_analytics
import pytest

TEST_ISRC = 'USUM71911283' # senorita

'''
SAMPLE RETURN: 
	analytics = {
		'spotify_reach': str(Quantity(spotify_reach)) if spotify_reach else None,
		'num_spotify_playlists': str(Quantity(num_spotify_playlist)) if num_spotify_playlist else None,
		'spotify_popularity': spotify_popularity,
		'cm_track_page': cm_track_page_url,
		'top_spotify_playlist_url':top_spotify_playlist_url,
		'thirty_sec_preview': thirty_sec_preview
	}
'''
def test_get_analytics():
	analytics = get_analytics(TEST_ISRC)

	assert analytics
	assert 'spotify_reach' in analytics
	assert 'num_spotify_playlists' in analytics
	assert 'spotify_popularity' in analytics
	assert 'cm_track_page' in analytics
	assert 'top_spotify_playlist_url' in analytics
	assert 'thirty_sec_preview' in analytics


