from afr.services.spotify import get_song_data_by_spotify_id, get_song_data_by_name, get_song_data_by_isrc, get_thirty_sec_preview_by_isrc
import pytest

TEST_ISRC_SENORITA = 'USUM71911283'
TEST_ISRC_BAD_GUY ='USUM71900764'
TEST_NAME = 'Panini'
TEST_SPOTIFY_ID_HELLO = '4sPmO7WMQUAf45kwMOtONw'

'''
Created on 8/23/19 by Nanette.
'''

def test_get_song_data_by_spotify_id():
	song_data = get_song_data_by_spotify_id(TEST_SPOTIFY_ID_HELLO)
	assert song_data
	assert 'isrc' in song_data
	assert 'embed_url' in song_data

def test_get_song_data_by_name():
	song_data = get_song_data_by_name(TEST_NAME)
	assert song_data
	assert 'isrc' in song_data
	assert 'embed_url' in song_data 

def test_get_song_data_by_isrc():
	song_data = get_song_data_by_isrc(TEST_ISRC_BAD_GUY)
	assert song_data
	assert 'embed_url' in song_data 
	
def test_get_thirty_sec_preview_by_isrc():
	assert get_thirty_sec_preview_by_isrc(TEST_ISRC_SENORITA)
	