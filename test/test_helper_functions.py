from afr.utils.helper_functions import *
import pytest


TEST_NAME_STITCHES = 'Stitches'

TEST_SPOTIFY_ID_UNINVITED = '0EDlULDWMq7mUhj28tvkol'

TEST_ISRC_I_DONT_CARE = 'GBAHS1900672'
TEST_ISRC_CHINA = 'QM6P41952433'
TEST_ISRC_SUNFLOWER = 'USUM71814888'

'''
Created on 8/23/19 by Nanette.
'''

def test_lookup_isrc():
	isrc = lookup_isrc(TEST_NAME_STITCHES)
	assert isrc

def test_lookup_isrc_by_spotify_id():
	isrc = lookup_isrc_by_spotify_id(TEST_SPOTIFY_ID_UNINVITED)
	assert isrc

def test_lookup_track_name():
	name = lookup_track_name(TEST_ISRC_I_DONT_CARE)
	assert name

def test_lookup_spotify_track_id():
	spotify_track_id = lookup_spotify_track_id(TEST_ISRC_CHINA)
	assert spotify_track_id

def test_lookup_artwork_url():
	artwork_url = lookup_artwork_url(TEST_ISRC_I_DONT_CARE)
	assert artwork_url

def test_lookup_artist_name():
	artist_name = lookup_artist_name(TEST_ISRC_SUNFLOWER)
	assert artist_name

def test_lookup_top_playlist_containing_isrc():
	playlist_id = lookup_top_playlist_containing_isrc(TEST_ISRC_CHINA)
	assert playlist_id

def test_lookup_cm_track_id():
	cm_track_id = lookup_cm_track_id(TEST_ISRC_I_DONT_CARE)
	assert cm_track_id

def test_lookup_spotify_reach():
	reach = lookup_spotify_reach(TEST_ISRC_CHINA)
	assert reach

def test_lookup_num_spotify_playlists():
	num_playlists = lookup_num_spotify_playlists(TEST_ISRC_SUNFLOWER)
	assert num_playlists 
	assert num_playlists > 0

def test_lookup_popularity():
	popularity = lookup_popularity(TEST_ISRC_SUNFLOWER)
	assert popularity
	assert popularity >= 0